include /usr/share/dpkg/pkg-info.mk
include /usr/share/dpkg/architecture.mk

PACKAGE=omping
PKGDIR=${PACKAGE}-${DEB_VERSION_UPSTREAM}
PKGSRC=${PACKAGE}_${DEB_VERSION_UPSTREAM}.orig.tar.gz

GITVERSION:=$(shell cat .git/refs/heads/master)

DEB=${PACKAGE}_${DEB_VERSION_UPSTREAM_REVISION}_${DEB_BUILD_ARCH}.deb
DBGDEB=${PACKAGE}-dbgsym_${DEB_VERSION_UPSTREAM_REVISION}_${DEB_BUILD_ARCH}.deb

DSC=${PACKAGE}_${DEB_VERSION_UPSTREAM_REVISION}.dsc

DEBS=${DEB} ${DBGDEB}

all: ${DEBS}

.PHONY: ${PKGDIR}
${PKGDIR}: ${PKGSRC}
	rm -rf ${PKGDIR}
	tar xf ${PKGSRC}
	rm -rf ${PKGDIR}/debian
	cp -a debian ${PKGDIR}/debian
	echo "git clone git://git.proxmox.com/git/pve-omping.git\\ngit checkout ${GITVERSION}" > ${PKGDIR}/debian/SOURCE

.PHONY: deb
deb: ${DEBS}
${DBGDEB}: ${DEB}
${DEB}: ${PKGDIR}
	cd ${PKGDIR}; dpkg-buildpackage -b -us -uc
	lintian ${DEB}

.PHONY: dsc
dsc: ${DSC}
${DSC}: ${PKGDIR}
	cd ${PKGDIR}; dpkg-buildpackage -S -us -uc -d
	lintian ${DSC}

.PHONY: upload
upload: ${DEBS}
	tar cf - ${DEBS} | ssh repoman@repo.proxmox.com -- upload --product pve --dist buster --arch ${DEB_BUILD_ARCH}

distclean: clean

.PHONY: clean
clean:
	rm -rf *~ *_${DEB_BUILD_ARCH}.deb *.changes *.dsc *.diff.gz *.buildinfo ${PKGDIR}

.PHONY: dinstall
dinstall: ${DEB}
	dpkg -i ${DEB}
